#pragma once

#include "Command.h"
#include <vector>

/*iterator class for classes that contains vector*/
template <typename T, typename U>
class Iterator{
	U * data_p_;
	typedef typename std::vector<T>::iterator iter_type;
	iter_type iter_;
public:
	
	/*sets pointer to container data and current iter pointer to begin*/
	Iterator(U* data) : data_p_(data){
		iter_ = data_p_->commands_.begin();
	};

	/*sets current iter position to begin*/
	void first(){
		iter_ = data_p_->commands_.begin();
	}

	/*checks is iter == end*/
	bool is_end(){
		return (iter_ == data_p_->commands_.end());
	}

	/*return current iter position*/
	iter_type current(){
		return iter_;
	}

	/*increase iter position*/
	void next(){
		iter_++;
	}
};

/*container create iterator for data that contain*/
Iterator<Command*, Round> *Round::create_iter()
{
	return new Iterator<Command*, Round>(this);
}