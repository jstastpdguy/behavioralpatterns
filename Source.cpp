#include "Command.h"
#include "State.h"
#include "Visitor.h"
#include "Observer.h"
#include "Iterator.h"

int main(){

	/*command example*/
	std::cout << "command example:\n\n";
	Enemy enemy;
	Archer archer;
	Warrior warrior;
	Mage mage;

	Round round_;

	round_.Add(new Command(&archer, &enemy, &Unit::SpecialAttack));
	round_.Add(new Command(&mage, &Unit::Wait));
	round_.Add(new Command(&warrior, &enemy, &Unit::Attack));

	round_.Start();

	round_.Add(new Command(&archer, &Unit::Wait));
	round_.UndoCommand();
	round_.Add(new Command(&mage, &enemy, &Unit::Attack));
	round_.Add(new Command(&warrior, &Unit::Defend));

	round_.Start();
	/**/

	/*state example*/
	std::cout << "\n\nstate example:\n\n";
	House house;

	house.ChangeState(new ForSale());

	std::cout << house << '\n';

	house.SellHouse();
	house.BuyHouse();

	std::cout << house <<'\n';

	house.BuyHouse();
	house.SellHouse();

	std::cout << house << '\n';

	/**/

	/*visitor example*/
	std::cout << "\n\nvisitor example:\n\n";

	CraftComponent *composite_items[3];

	composite_items[0] = new CompositeComponent("The Sword of Thousand Truths");
	composite_items[1] = new CompositeComponent("Sword blade");
	composite_items[2] = new CompositeComponent("Sword grip");

	AddComponentVisitor add_visitor;
	RemoveComponentVisitor remove_visitor;

	CraftComponent* metall = new SimpleComponent("metall");

	composite_items[2]->accept(add_visitor, new SimpleComponent("Skull"));
	composite_items[2]->accept(add_visitor, new SimpleComponent("metall"));

	composite_items[1]->accept(add_visitor, new SimpleComponent("runes"));
	composite_items[1]->accept(add_visitor, new SimpleComponent("metall"));
	composite_items[1]->accept(add_visitor, metall);

	composite_items[0]->accept(add_visitor, composite_items[1]);
	composite_items[0]->accept(add_visitor, composite_items[2]);

	std::cout << "result: " << composite_items[0]->Operation() << '\n';

	composite_items[1]->accept(remove_visitor, metall);

	std::cout << "result: " << composite_items[0]->Operation() << '\n';
	delete composite_items[0];
	/**/

	/*observer example*/
	std::cout << "\n\nobserver example:\n\n";

	Player player(1);
	player.levelUp();
	player.levelUp();
	player.levelUp();
	/**/

	/*iterator example*/
	std::cout << "\n\niterator example:\n\n";

	round_.Add(new Command(&archer, &enemy, &Unit::SpecialAttack));
	round_.Add(new Command(&mage, &Unit::Wait));
	round_.Add(new Command(&warrior, &enemy, &Unit::Attack));

	Iterator<Command*, Round> *iter = round_.create_iter();

	for(iter->first(); !iter->is_end(); iter->next()){
		(**iter->current()).execute();
	}

	round_.Start();

	return 0;
}