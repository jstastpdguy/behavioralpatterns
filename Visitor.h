#pragma once

#include <iostream>
#include <list>

/*
*  class provides general methods for composit and simle objects
*/
class CraftComponent{
protected:
	CraftComponent *parent_;
	std::string name_;
public:
	virtual ~CraftComponent(){}

	/*set name of object*/
	CraftComponent(std::string const & name) : name_(name){
		parent_ = nullptr;
	};

	/*set reference to parent in parent_ field*/
	void SetParent(CraftComponent *parent){
		this->parent_ = parent;
	}

	/*return parent of object*/
	CraftComponent * GetParent() const{
		return this->parent_;
	}

	/*return complexity of object*/
	virtual bool IsComposite() const{
		return false;
	}

	/*some operation */
	virtual std::string Operation() const = 0;

	/*accept visitor logic to craft component*/
	virtual void accept(class Visitor &visitor, CraftComponent *component) = 0;
};

/* simple component hasn't nested components
	usually do all work, whereas other components delegate work
*/
class SimpleComponent : public CraftComponent{
public:

	SimpleComponent(std::string const && name) : CraftComponent(name){};

	std::string Operation() const override{
		return name_;
	}

	/*accept visitor's logic*/
	void accept(class Visitor &visitor, CraftComponent *component) override;
};

/*
	composite components may contain simple components and other composite components
	delegate work to children
*/
class CompositeComponent : public CraftComponent{
protected:
	std::list<CraftComponent*> children_;

public:

	CompositeComponent(std::string const && name) : CraftComponent(name){}

	/*add new component to children list
	and set its parent to self
	*/
	void Add(CraftComponent* component) {
		this->children_.push_back(component);
		component->SetParent(this);
	}

	/*remove component from children list
	and set its parent to nullptr
	*/
	void Remove(CraftComponent * comp) {
		children_.remove(comp);
		comp->SetParent(nullptr);
	}

	bool IsComposite() const override{
		return true;
	}

	/*delegate work to all children and sum it's results*/
	std::string Operation() const override{
		std::string result;

		for(const CraftComponent* c : children_){
			if(c == children_.back()){
				result += c->Operation();
			}else{
				result += c->Operation() + "+";
			}
		}

		return name_ + ": (" + result + ')';
	}

	/*accept visitor's logic*/
	void accept(class Visitor &visitor, CraftComponent *component) override;

	/*delete all children*/
	~CompositeComponent(){
	
		for (auto i : children_){
			delete i;
		}

		children_.clear();
	}
};

/*visitor interface for simple and composite components*/
class Visitor{
public:
	/*for simple components*/
	virtual void visit(SimpleComponent *, CraftComponent *) = 0;
	/*for composite*/
	virtual void visit(CompositeComponent *, CraftComponent *) = 0;
};

/*implementation of visitor interface which adds components to more complex*/
class AddComponentVisitor : public Visitor{
public:

	/*simple components can't add other components*/
	void visit(SimpleComponent * simple, CraftComponent * general) override{
		/*do nothing when client try add component to simple component*/
	}

	/*adds components*/
	void visit(CompositeComponent * composite, CraftComponent * simple) override{
		composite->Add(simple);
	}
};

/*implementation of visitor interface which removes components from more complex*/
class RemoveComponentVisitor : public Visitor{
public:

	/*simple components can't remove other components*/
	void visit(SimpleComponent * simple, CraftComponent * general) override{
		/*do nothing when client try add component to simple component*/
	}

	/*removes components*/
	void visit(CompositeComponent * composite, CraftComponent * simple) override{
		composite->Remove(simple);
	}
};

/*accept visitor's logic*/
void SimpleComponent::accept(class Visitor &visitor, CraftComponent *component){
	visitor.visit(this, component);
}

/*accept visitor's logic*/
void CompositeComponent::accept(class Visitor &visitor, CraftComponent *component){
	visitor.visit(this, component);
}