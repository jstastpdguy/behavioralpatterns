#pragma once
#include <iostream>
#include <map>

/*observer interface*/
class IObserver{
public:
	virtual ~IObserver(){};
	virtual void update(int) = 0;
};

/*subject interface*/
class ISubject{
public:
	virtual ~ISubject(){};
	virtual void notify() = 0;
};

/*enum to indicate type of listener in map*/
enum class ListenerType{
	STORE = 0,
	QUEST
};

/*observer that update assortment of items depending on the player level */
class ItemStore : public IObserver{
public:
	virtual void update(int level) {
		std::cout << "the assortment of the store has been updated\n";
		std::cout << "items level: " << ((level - 2) > 0 ? (level - 2) : 1) << " - " << level + 2 << '\n';
	};
};

/*observer that update available quest list depending on the player level */
class QuestGiver : public IObserver{
	virtual void update(int level) {
		std::cout << level << " level quests are avaliable\n";
	};
};

/*listeners manager which updates all subscribed listeners*/
class Listeners{
	std::map<ListenerType, IObserver*> listeners_;
public:

	/*adds new listener to map*/
	void subscribe(ListenerType &&key, IObserver * obs){
		listeners_[key] = obs;
	}

	/*remove listener from map by key*/
	void unsubscribe(ListenerType &&key, IObserver * obs){
		listeners_.erase(key);
	}

	/*update all listeners in map*/
	void update(int level){
		for (auto listener : listeners_){
			listener.second->update(level);
		}
	}

	/*free memory*/
	~Listeners(){
		for( auto listener : listeners_){
			if(listener.second){
				delete listener.second;
				listener.second = nullptr;
			}
		}

		listeners_.clear();
	}
};

/*subject class that notify all listeners from listeners class*/
class Player : public ISubject{
	int player_level_;
	Listeners listeners_;
public:

	/*subscribe needed listeners*/
	Player(int level) : player_level_(level){
		listeners_.subscribe(ListenerType::STORE, new ItemStore);
		listeners_.subscribe(ListenerType::QUEST, new QuestGiver);
	};

	/*notify listeners when it needed*/
	void notify() override {
		listeners_.update(player_level_);
	};

	/*increase level of player end call notify*/
	void levelUp(){
		player_level_++;
		notify();
	}
};