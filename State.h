#pragma once
#include <iostream>

/*state interface*/
class State{
public:
	virtual bool BuyHouse() = 0;
	virtual bool SellHouse() = 0;
};

/*state of house that hasn't been owned*/
class ForSale : public State{
public:

	/*changes state to owned*/
	bool BuyHouse() override{
		std::cout << "House was bought!\n";
		return true;
	}

	/*tells what it hasn't been already owned*/
	bool SellHouse() override{
		std::cout << "You can't sell house that you don't own!\n";	
		return false;
	}
};

/*state of house that has been owned*/
class Owned : public State{
public:

	/*tells what it has been already owned*/
	bool BuyHouse() override{
		std::cout << "You can't buy house that already owned!\n";
		return false;
	}

	/*changes state to for sale*/
	bool SellHouse() override{
		std::cout << "Your house was sold!\n";
		return true;
	}
};

/*context that delegates some work to state*/
class House{
protected:

	/*pointer to current state*/
	State* state_;
public:

	/*init state with nullptr for correct work of change state*/
	House() : state_(nullptr){};

	/*changes state of house*/
	void ChangeState(State * state){
		if (state_){
			delete state_;
			state_ = nullptr;
		}
		state_ = state;
	}

	/*checks current state end change own current state to new*/
	void BuyHouse(){
		if(state_->BuyHouse()){
			ChangeState(new Owned());
		}
	}

	/*checks current state end change own current state to new*/
	void SellHouse(){
		if(state_->SellHouse()){
			ChangeState(new ForSale());
		}
	}

	/*output to ostream current state*/
	friend std::ostream& operator <<(std::ostream& os, House const & house){
		if(dynamic_cast<Owned*>(house.state_)){
			os << "House owned.";
		}else os << "House for sale.";

		return os;
	}
};