#pragma once
#include <memory>
#include <string>
#include <iostream>
#include <list>
#include <vector>

/*interface for all units on battlefield*/
class Unit{
protected:
	std::unique_ptr<std::string> name_;
public:

	Unit(){};

	Unit(std::string const& name){
		name_ = std::make_unique<std::string>(name);
	}

	virtual void Attack(Unit const& enemy) = 0;
	virtual void Wait() = 0;
	virtual void Defend() = 0;
	virtual void SpecialAttack(Unit const& enemy) = 0;

	friend std::ostream& operator << (std::ostream & os, Unit const& unit){
		return os << *unit.name_;
	}
};

/*archer class which implements commands of interface*/
class Archer : public Unit{
public:

	Archer() : Unit("Archer") {};

	void Attack(Unit const& enemy) override{
		std::cout << *name_ << " hits the " << enemy << " with well-aimed shot!\n";
	};

	void Wait() override{
		std::cout << *name_ << " took a break to think...\n";
	};

	void Defend() override{
		std::cout << *name_ << " try to defend himself from enemy attacks\n";
	};

	void SpecialAttack(Unit const& enemy) override{
		std::cout << *name_ << " hits the " << enemy << " with his special ability!\n";
	};
};

/*warrior class which implements commands of interface*/
class Warrior : public Unit{
public:

	Warrior() : Unit("Warrior") {};

	void Attack(Unit const& enemy) override{
		std::cout << *name_ << " raises his sword over " << enemy << "'s head !\n";
	};

	void Wait() override{
		std::cout << *name_ << " took a break to think...\n";
	};

	void Defend() override{
		std::cout << *name_ << " try to defend himself from enemy attacks\n";
	};

	void SpecialAttack(Unit const& enemy) override{
		std::cout << *name_ << " hits the " << enemy << " with his special ability!\n";
	};
};

/*mage class which implements commands of interface*/
class Mage : public Unit{
public:

	Mage() : Unit("Mage") {};

	void Attack(Unit const& enemy) override{
		std::cout << *name_ << " concentrates energy in the staff and shoots to the " << enemy << "\n";
	};

	void Wait() override{
		std::cout << *name_ << " took a break to think...\n";
	};

	void Defend() override{
		std::cout << *name_ << " try to defend himself from enemy attacks\n";
	};

	void SpecialAttack(Unit const& enemy) override{
		std::cout << *name_ << " hits the " << enemy << " with his special ability!\n";
	};
};

/*enemy class which implements commands of interface*/
class Enemy : public Unit{
	
public:

	Enemy() : Unit("Orc's King") {};

	Enemy(std::string const& name) : Unit(name){};

	void Attack(Unit const& enemy) override{};
	void Wait() override{};
	void Defend() override{};
	void SpecialAttack(Unit const& enemy) override{};

	friend std::ostream& operator << (std::ostream &os, Enemy const & en){
		return os << en.name_;
	}
};

/*incapsulate methods of unit classes*/
class Command{
	Unit *unit_;
	Unit *target_;

	/*pointer to units methods, which has no target, or uses by itself*/
	void (Unit::*defensive)();
	/*pointer to units methods, which has target*/
	void (Unit::*offensive)(Unit const&);
public:

	/*constructor for methods, which has no target*/
	Command(Unit *unit, void (Unit::*ptr)()){
		unit_ = unit;
		target_ = nullptr;
		offensive = nullptr;
		defensive = ptr;
	}

	/*constructor for methods, which has target*/
	Command(Unit *unit, Unit *target, void (Unit::*ptr)(Unit const &)){
		unit_ = unit;
		target_ = target;
		offensive = ptr;
		defensive = nullptr;
	}

	/*execute unit's method, depending on method's pointer*/
	void execute(){
		if(defensive){
			(unit_->*defensive)();
		}else{
			(unit_->*offensive)(*target_);
		}		
	};

	Command* test(){
		return this;
	}
};

template <typename T, typename U> class Iterator;

/*game round contains command list, and execute it on start*/
class Round{
	int count = 1;
	std::vector<Command*> commands_;
public:

	/*execute all commands, and delets it after that
	*clear vector
	*/
	void Start(){
		for (auto command : commands_){
			command->execute();
			delete command;
			command = nullptr;
		}

		std::vector<Command*>().swap(commands_);
		std::cout << "\nRound " << count++ << " end!\n\n"; 
	}

	/*add command to commands list*/
	void Add(Command * com){
		commands_.push_back(com);
	}

	/*removes command from list*/
	void UndoCommand() {
		if(commands_.size())
			commands_.pop_back();
	}

	/*methods for iterator realisation*/
	friend class Iterator<Command*,Round>;
	class Iterator<Command*, Round> * create_iter();
};